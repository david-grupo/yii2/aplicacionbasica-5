<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trabajadores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $fechaNacimiento
 * @property string $foto
 * @property int $delegacion
 *
 * @property Delegacion $delegacion0
 */
class Trabajadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trabajadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaNacimiento'], 'safe'],
            [['delegacion'], 'integer'],
            [['nombre', 'apellidos', 'foto'], 'string', 'max' => 255],
            [['delegacion'], 'exist', 'skipOnError' => true, 'targetClass' => Delegacion::className(), 'targetAttribute' => ['delegacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'foto' => 'Foto',
            'delegacion' => 'Delegacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelegacion0()
    {
        return $this->hasOne(Delegacion::className(), ['id' => 'delegacion']);
    }
    
    
    public function getDelegacionSantander(){
        return $this->hasOne(Delegacion::className(), ['id' => 'delegacion'])
                    ->andWhere(["poblacion"=>"Santander"]);
    }
    public function getNombreCompleto()
    {
        return $this->nombre . " " . $this->apellidos;
    }
    
    public static function getConsulta1()
    {
        return Yii::$app
                ->db
                ->createCommand('Select concat(nombre,apellidos)
                         as nombreCompleto FROM trabajadores')
                ->queryAll();
        
    }
    public static function getConsulta2()
    {
        return (new \yii\db\Query)
                
                ->select(['concat(nombre,apellidos)
                         as nombreCompleto'])
                ->from('trabajadores')
                ->all();
        
    }
    public static function getConsulta3()
    {
        return self::find()
        ->select(['*,concat(nombre," ",apellidos)as nombreCompleto'])
        ->asArray()
        ->all();        
    }
    public static function getConsulta4()
    {
        $listado=self::find()->all();
        return \yii\helpers\ArrayHelper::toArray($listado,[
            'app\models\Trabajadores'=>[
                'id',
                'nombre',
                'apellidos',
                'nombreCompleto' => function($listado){
                    return $listado->nombre." ".$listado->apellidos;
                }
            ]
        ]);      
    }
}
