

<?php
use yii\helpers\Html;
?>
<div class=" row row-flex row-flex-wrap">
    <?php
foreach($modelos as $modelo)
{
?>
<div class="col-sm-4 col-md-3 flex-col">
    <div class="thumbnail">
     
      <div class="caption">
        <h3><?=$modelo->id; ?></h3>
        <ul>
            <li>Nombre: <?= $modelo->nombre; ?></li>
            <li>Apellidos: <?= $modelo->apellidos; ?></li>
        </ul>
        <figure>
         <?=Html::img("@web/imgs/$modelo->foto",[
             'class'=>'img-responsive',
         ]); ?>
        </figure>
      </div>
    </div>
  </div>
<?php
}

?>

</div>
