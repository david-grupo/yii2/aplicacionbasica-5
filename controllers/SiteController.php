<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Trabajadores;
use app\models\Delegacion;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta12()
    {
        Trabajadores::find()->all();
        
        
        return $this->render('consulta12',[
            'modelos'=>Trabajadores::find()->all(),
        ]);
    }
    
    public function actionConsulta14()
    {
        // consulta con active record
        $a=Delegacion::find()->all();
        
        // consulta con command
        $b=Yii::$app
                ->db
                ->createCommand("Select * from delegacion")
                ->queryAll();
        
        // consulta con query builder
        $query=new \yii\db\Query;
        $c= $query->select('*')               
                  ->from('delegacion')
                  ->all();
                          
        return $this->render('consulta14',[
            "uno"=>$a,
            "dos"=>$b,
            "tres"=>$c,
            ]);
            
    }
    public function actionConsulta17()
    {
        //activerecord
        $a=Delegacion::find()->where(['poblacion'=>"Santander"])->all();
        
        $b=Trabajadores::find()->where(["delegacion"=>1])->all();
        
        $c=Trabajadores::find()->orderBy('nombre')->all();
        
        $h=Trabajadores::find()->where(['fechaNacimiento'=>'null'])->all();
        
        //create command
        $d=Yii::$app
                ->db
                ->createCommand("Select * from delegacion where poblacion='Santander'")
                ->queryAll();
        $e=Yii::$app
                ->db
                ->createCommand("Select * from trabajadores where delegacion=1")
                ->queryAll();
        $f=Yii::$app
                ->db
                ->createCommand("select * from trabajadores order by nombre")
                ->queryAll();
        $g=Yii::$app
                ->db
                ->createCommand("Select * from trabajadores where fechanacimiento=null")
                ->queryAll();
        return $this->render('consulta17',[
            "uno"=>$a,
            "dos"=>$b,
            "tres"=>$c,
            "cuatro"=>$d,
            "cinco"=>$e,
            "seis"=>$f,
            "siete"=>$g,
            "ocho"=>$h,
        ]);
    }
    public function actionConsulta20()
    {
        /* Ramon 
        echo "<pre>";
        var_dump(Trabajadores::find()
                ->with('delegacion0')
                ->asArray()->all());
        echo"</pre>";
          
        para conocer un elemento concreto usamos la propiedad que nos crea el modelo 
         'delegacion0'
        Trabajadores::find()->all()[0]->delegacion0->nombre;
                */
        
        /* ramon
         Trabajadores::find()
           ->with('delegacion0')
           ->all();
         */
        //------------------------------------------------------------------------------
        // modelo consulta ramon sobre delegaciones con poblacion distinta a santander
         $a=[];
         $a=Delegacion::find()
              ->where(['<>','poblacion','santander'])
              ->andWhere(["is not","direccion",null])
                 //o ->where('poblacion not like "santander" and direccion is not null')
              ->all();
         
         /*modelo mio sobre delegaciones con poblacion distinta a santander
        $a=Yii::$app
                ->db
                ->createCommand("Select * from delegacion where poblacion not like 'santander'
          and direccion is not null")
                ->queryAll();*/
         
        //---------------------------------------------------------------------------------- 
         //modelo ramon sobre listar todos los trabajadores de las delegaciones de santander
         
         $b[]=Trabajadores::find()
                 ->joinWith("delegacion0")
                 ->where(["poblacion"=>"santander"])
                 ->asArray()
                 ->all();
         
         
         /*   misma consulta con map (optimizada)
         $b=Delegacion::find();
            ->select("id")
                    ->where(["poblacion"=>"santander"])
                    ->asArray()
                    ->all();
          $b= \yii\helpers\ArrayHelper::map($a,"id","id")
            $datos[]=Trabajadores::find()
                    ->where("in","delegacion",$a)
                    ->asArray()
                    ->all()
           
          
          */
         /* modelo mio de la consulta anterior
        $b=Yii::$app
                ->db
                ->createCommand("SELECT trabajadores.nombre,apellidos FROM trabajadores JOIN
                    delegacion ON(delegacion.id=trabajadores.delegacion)
                        WHERE delegacion=1;")
                ->queryAll();
          
          */
        //--------------------------------------------------------------------------------
         //modelo ramon sobre listar trabajadores y delegaciones de los trabajadores con foto
        /*$c=Trabajadores::find()
                 ->with('delegacion0')
                 ->where('foto is no null')
                 ->all();*/
         
         
         
         
         
         //modelo mio de la consulta anterior
        $c=Yii::$app
                ->db
                ->createCommand("SELECT trabajadores.nombre,apellidos,delegacion,
                    delegacion.nombre,delegacion.poblacion,foto FROM trabajadores JOIN
                    delegacion ON(delegacion.id=trabajadores.delegacion) WHERE foto IS NOT null;")
                ->queryAll();
        
        //------------------------------------------------------------------------
        /*modelo consulta delegaciones sin trabajadores*/
        /*$d=Delegacion::find()
                ->joinWith("trabajadores t")
                ->where(["t.delegacion"=>null])
                ->all();*/
        
        $d=Yii::$app
                ->db
                ->createCommand("SELECT * FROM delegacion left JOIN
                  trabajadores ON(delegacion.id=trabajadores.delegacion)
                  WHERE delegacion IS NOT NULL;")
                ->queryAll();
        
        /*consulta sobre el numero de trabajadores por delegacion*/
        /* consulta ramon
         $delegaciones=Delegacion::find();
          ->with("trabajadores")
          ->asArray()
          ->all();
         foreach($delegaciones as $delegacion){echo(count($delegacion["trabajadores]))}
         */
        //mia
         $e=Yii::$app
                ->db
                ->createCommand("SELECT delegacion.id,delegacion.nombre,COUNT(trabajadores.nombre)
                    FROM delegacion LEFT JOIN
  trabajadores ON(delegacion.id=trabajadores.delegacion) 
  GROUP BY delegacion ORDER BY delegacion.id asc")
                ->queryAll();
         
        
        
        return $this->render('consulta20',[
            "uno"=>$a,
            "dos"=>$b,
            "tres"=>$c,
            "cuatro"=>$d,
            "cinco"=>$e,
        ]);
    }
    public function actionConsulta22()
    {
        $a=[];$b=[];$c=[];
        $a[]=Trabajadores::getConsulta1();
        $b[]=Trabajadores::getConsulta2();
        $c[]=Trabajadores::getConsulta3();
        $d[]=Trabajadores::getConsulta4();
      return $this->render('consulta22',[
          "a"=>$a,
          "b"=>$b,
          "c"=>$c,
          "d"=>$d,
      ]);  
        
    }
    
}
