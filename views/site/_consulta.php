<?php
use yii\helpers\Html;
?>
<div class="col-sm-4 col-md-3 flex-col">
    <div class="thumbnail">
     
      <div class="caption">
        <h3><?=$modelo->id; ?></h3>
        <ul>
            <li>Nombre: <?= $modelo->nombre; ?></li>
            <li>Apellidos: <?= $modelo->apellidos; ?></li>
        </ul>
        <figure>
         <?=Html::img("@web/imgs/$modelo->foto",[
             'class'=>'img-responsive',
         ]); ?>
        </figure>
      </div>
    </div>
  </div>
